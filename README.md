#spring-security-4

Este Repositório contém exemplos de uso do Spring Security.

## Exemplo com LDAP usando AD da Microsoft


## Active Directory Lightweight Directory Services no Windows7 64bits

[https://dzone.com/articles/getting-started-active](https://dzone.com/articles/getting-started-active)

O arquivo **Windows6.1-KB975541-x64.msu** que é o Setup para Windows 7 encontra-se neste repositório.

## O que temos no repositório

* Exemplo básico de uso do Spring Security com adaptações simples.
* Informações de como integrar o LDAP Directory Services com o Spring Security

## Veja o que você precisa para usar estes códigos 

* Java Development Kit 1.8 pode ser baixado do site da Oracle 
* Spring Tool Suite **é opcional** e pode ser baixado de http://www.springsource.org/sts
* Apache Tomcat 8 pode ser baixado de http://tomcat.apache.org/
* Apache Maven 3 pode ser baixado de http://maven.apache.org/

## Para quem se destina este repositório

Desenvolvedores Java com conhecimento básico de criação de aplicação Java e de XML
