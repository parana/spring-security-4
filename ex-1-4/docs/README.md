
## Implementando o Atuator

[Ver post neste Blog - http://javabeat.net/spring-boot-actuator/](http://javabeat.net/spring-boot-actuator/)

### pom.xml

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>
		

### ServerEndpoint

```java
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.actuate.endpoint.Endpoint;
import org.springframework.stereotype.Component;

@Component
public class ServerEndpoint implements Endpoint<List<String>> {

	public String getId() {
		return "server";
	}

	public List<String> invoke() {
		List<String> serverDetails = new ArrayList<String>();
		try {
			serverDetails.add("Server IP Address : " +
			InetAddress.getLocalHost().getHostAddress());
			serverDetails.add("Server OS : " + System.getProperty("os.name").toLowerCase());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return serverDetails;
	}

	public boolean isEnabled() {
		return true;
	}

	public boolean isSensitive() {
		return false;
	}
}
```

### ShowEndpoints

```
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.AbstractEndpoint;
import org.springframework.boot.actuate.endpoint.Endpoint;
import org.springframework.stereotype.Component;

@Component
public class ShowEndpoints extends AbstractEndpoint<List<Endpoint>>{

	private List<Endpoint> endpoints;

    @Autowired
    public ShowEndpoints(List<Endpoint> endpoints) {
        super("showEndpoints");
        this.endpoints = endpoints;
    }

    public List<Endpoint> invoke() {
        return this.endpoints;
    }
}
```

### CustomHealthCheck


```
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class CustomHealthCheck implements HealthIndicator {
	public Health health() {
		int errorCode = 0;
		if (errorCode != 1) {
			return Health.down().withDetail("Error Code", errorCode).build();
		}
		return Health.up().build();
	}

}
```


### application.properties


```
management.port=8081
management.context-path=/details
management.security.enabled=true

security.basic.enabled=true
security.user.name=admin
security.user.password=admin

endpoints.health.id=health
endpoints.health.sensitive=true
endpoints.health.enabled=true

endpoints.metrics.id=metrics
endpoints.metrics.sensitive=true
endpoints.metrics.enabled=true

endpoints.server.id=server
endpoints.server.sensitive=false
endpoints.server.enabled=true

endpoints.info.id=info
endpoints.info.sensitive=false
endpoints.info.enabled=true
info.app.name=Spring Actuator Example
info.app.description=Spring Actuator Working Examples
info.app.version=0.0.1-SNAPSHOT
```




		