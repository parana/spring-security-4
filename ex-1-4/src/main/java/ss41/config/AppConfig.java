package ss41.config;

import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import ss41.web.servlet.MyServlet;

@EnableWebMvc
@Configuration
@Import({ SecurityConfig.class })
public class AppConfig extends WebMvcConfigurerAdapter {

	// It'll get mapped to /{beanName}/
	// @Bean(name = "my_servlet")
	// public Servlet my_servlet() {
	// return new MyServlet();
	// }

	// if you actually want it mapped to /something/* rather than /something/
	// you will need to use ServletRegistrationBean
	@Bean
	public ServletRegistrationBean servletRegistrationBean() {
		ServletRegistrationBean srb = new ServletRegistrationBean(new MyServlet(), "/my-servlet/*");
		return srb;
	}

	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/JSP/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}
}