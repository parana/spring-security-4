package ss41.mngt;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.AbstractEndpoint;
import org.springframework.boot.actuate.endpoint.Endpoint;
import org.springframework.stereotype.Component;

import ss41.Application;

@Component
public class ShowEndpoints extends AbstractEndpoint<List<Endpoint<?>>> {

	private List<Endpoint<?>> endpoints;

	@Autowired
	public ShowEndpoints(List<Endpoint<?>> endpoints) {
		super("showEndpoints");
		System.out.println("° ° ° ° °   creating ShowEndpoints object");
		this.endpoints = endpoints;
	}

	public List<Endpoint<?>> invoke() {
		System.out.println("° ° ° ° °   invoke() running");
		Application.getRolesForUserAuthenticated();

		for (Endpoint<?> endpoint : endpoints) {
			System.out.println("° ° ° ° °   Endpoint class = " + endpoint.getClass().getCanonicalName());
		}

		return this.endpoints;
	}
}
