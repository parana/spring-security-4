package ss41.mngt;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import ss41.Application;

@Component
public class CustomHealthCheck implements HealthIndicator {
	public Health health() {
		Application.getRolesForUserAuthenticated();
		int errorCode = 0;
		if (errorCode != 1) {
			return Health.down().withDetail("Error Code", errorCode).build();
		}
		return Health.up().build();
	}

}