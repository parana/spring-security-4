package ss41.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		resp.setHeader("Cache-Control", "no-cache");
		resp.setHeader("pragma", "no-cache");
		PrintWriter out = resp.getWriter();
		out.println("<html>");
		out.println("<body>");
		out.println("  <h5>Hello MyServlet using GET ");
		out.println("    Method whith  ContentType = \"text/html\"");
		out.println("    and \"pragma\" = \"no-cache\"</h5>");
		out.println("</body>");
		out.println("</html>");
	}
}
