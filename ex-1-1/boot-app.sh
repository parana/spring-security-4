#!/bin/bash

set -e

APP_NAME=spring-boot-spring-security-jpa
# Projeto Eclipse : spring-boot-spring-security-jpa
if [ $1 = 'build' ];
then
  mvn clean package install
fi
# 
mvn spring-boot:run -Drun.jvmArguments='-Dserver.port=8088'

