package ss41.service;

import org.springframework.data.jpa.repository.JpaRepository;

import ss41.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);
}
