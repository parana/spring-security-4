package ss41.service;

import ss41.domain.User;

public interface UserService {
	User getUserByUsername(String username);
}