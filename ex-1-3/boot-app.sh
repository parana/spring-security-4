#!/bin/bash

set -e

APP_NAME=spring-boot-spring-security-ldap-url
# Projeto Eclipse : ss4_ex-1-3
if [ $1 = 'build' ];
then
  mvn clean package install
fi
# 
mvn spring-boot:run -Drun.jvmArguments='-Dserver.port=8088'

