package ss41.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

import ss41.ErrorJson;

// See this post: https://gist.github.com/jonikarppinen/662c38fb57a23de61c8b

@RestController
public class SimpleErrorController implements ErrorController {

	private static final String PATH = "/error";

	public String error() {
		return "Error handling";
	}

	@Value("${debug}")
	private boolean debug;

	@Autowired
	private ErrorAttributes errorAttributes;

	@RequestMapping(value = PATH)
	ErrorJson error(HttpServletRequest request, HttpServletResponse response) {
		ErrorJson errorJson = new ErrorJson(response.getStatus(), getErrorAttributes(request, debug));

		System.out.println("° ° ° ° errorJson = " + errorJson);

		// Appropriate HTTP response code (e.g. 404 or 500) is automatically set
		// by Spring. Here we just define response body.
		return errorJson;
	}

	@Override
	public String getErrorPath() {
		return PATH;
	}

	private Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace) {
		RequestAttributes requestAttributes = new ServletRequestAttributes(request);
		return errorAttributes.getErrorAttributes(requestAttributes, includeStackTrace);
	}
}