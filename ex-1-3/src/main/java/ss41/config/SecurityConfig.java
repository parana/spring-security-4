package ss41.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.annotation.web.configurers.FormLoginConfigurer;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.AuthorizedUrl authorizedUrl = null;
		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry urlRegistry = null;
		urlRegistry = http.authorizeRequests();
		authorizedUrl = urlRegistry.antMatchers("/admin/**");
		urlRegistry = authorizedUrl.access("hasRole('ROLE_ADMIN') or hasRole('ROLE_DEVELOPERS')");
		authorizedUrl = urlRegistry.antMatchers("/super/**");
		urlRegistry = authorizedUrl.access("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGERS')");
		HttpSecurity httpSecurity = urlRegistry.and();
		FormLoginConfigurer<HttpSecurity> formLoginConfigurer = httpSecurity.formLogin();
		System.out.println("° ° ° ° formLoginConfigurer = " + formLoginConfigurer.toString());
	}

	@Configuration
	protected static class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {
		@Value("${soma.ldap.port}")
		private Integer ldapPort;

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.springframework.security.config.annotation.authentication.
		 * configurers.GlobalAuthenticationConfigurerAdapter#init(org.
		 * springframework.security.config.annotation.authentication.builders.
		 * AuthenticationManagerBuilder)
		 */
		@Override
		public void init(AuthenticationManagerBuilder auth) throws Exception {
			// Exemplo de import de LDIF com grupo e respectivos usuários
			//
			// dn: cn=developers,ou=groups,dc=example,dc=com
			// objectclass: top
			// objectclass: groupOfUniqueNames
			// cn: developers
			// ou: developer
			// uniqueMember: uid=ben,ou=people,dc=example,dc=com
			// uniqueMember: uid=bob,ou=people,dc=example,dc=com
			// uniquemember: uid=test1,ou=people,dc=example,dc=com

			System.out.println("° ° ° ° init running. auth = " + auth);

			String baseDn = "dc=example,dc=com";
			String ldapServerHost = "springframework.org";
			String urlLdapString = "ldap://" + ldapServerHost + ":" + ldapPort + "/" + baseDn;
			System.out.println("° ° ° ° urlLdapString = " + urlLdapString);

			String userDnPattern = "uid={0},ou=people";
			String groupSearchBase = "ou=groups";
			auth.ldapAuthentication().userDnPatterns(userDnPattern).groupSearchBase(groupSearchBase).contextSource()
					.url(urlLdapString);
		}
	}
}
