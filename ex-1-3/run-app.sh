#!/bin/bash

set -e

APP_NAME=spring-boot-spring-security-ldap-url
# Projeto Eclipse : ss4_ex-1-3
if [ $1 = 'build' ];
then
  mvn clean
  mvn package && rm -rf tomcat8x/webapps/$APP_NAME*
  cp target/$APP_NAME.war tomcat8x/webapps/$APP_NAME.war
fi
# 
rm -rf tomcat8x/logs/*.log 
rm -rf tomcat8x/logs/*.txt
cd tomcat8x/bin/
./run-tomcat.sh

