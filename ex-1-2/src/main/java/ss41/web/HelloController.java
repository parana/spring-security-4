package ss41.web;

import java.util.Map;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
// import org.springframework.security.core.userdetails.UserDetails;
// import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ss41.Application;

@Controller
public class HelloController {

	// @Autowired
	// UserDetailsService userService;

	@RequestMapping(value = { "/", "/hello**" }, method = RequestMethod.GET)
	public ModelAndView welcomePage() {
		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security Example");
		model.addObject("message", "This is Hello World!");
		model.setViewName("hello");
		return model;

	}

	@RequestMapping(value = "/admin**", method = RequestMethod.GET)
	public ModelAndView adminPage() {
		// UserDetails userDetails = userService.loadUserByUsername("admin");
		// System.out.println(userDetails.getUsername());
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security Example");
		model.addObject("message", "Logged In as  " + name + "!");
		model.setViewName("admin");
		return model;
	}

	@RequestMapping(value = "/super**", method = RequestMethod.GET)
	public ModelAndView dbaPage() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security Example");
		model.addObject("message", "Logged In as  " + name + "!");
		model.setViewName("admin");
		return model;
	}

	@RequestMapping(value = "/logout**", method = RequestMethod.GET)
	public ModelAndView logoutPage() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		SecurityContextHolder.getContext().setAuthentication(null);
		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security Example");
		model.addObject("message", name + " was Logged out !");
		model.setViewName("/logout-view");
		return model;
	}

	@RequestMapping(value = "/debug**", method = RequestMethod.GET)
	public ModelAndView errorPage() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth instanceof UsernamePasswordAuthenticationToken) {
			System.out.println("° ° ° ° °   UsernamePasswordAuthenticationToken Object   ° ° ° ° °");
		}
		System.out.println("° ° ° ° °   auth = " + auth);

		String name = auth.getName();
		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security Example - Debug Page");
		model.addObject("message", name + " is Logged but you get an error !");
		WebAuthenticationDetails authDetails = (WebAuthenticationDetails) auth.getDetails();
		model.addObject("message",
				"remoteAddress = " + authDetails.getRemoteAddress() + ", sessionId = " + authDetails.getSessionId()
						+ ", authorities = " + auth.getAuthorities() + ", principal = "
						+ Application.getPrincipalStringRepresentation(auth.getPrincipal()) + ", isAuthenticated = "
						+ auth.isAuthenticated());
		model.setViewName("/debugpage");
		return model;
	}
	
	@RequestMapping("/foo")
	public String foo(Map<String, Object> model) {
		throw new RuntimeException("Foo");
	}
}