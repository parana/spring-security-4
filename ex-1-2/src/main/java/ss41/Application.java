package ss41;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.directory.api.ldap.model.constants.LdapSecurityConstants;
// import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.apache.directory.api.ldap.model.password.PasswordUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.LdapUserDetails;

// @EnableJpaRepositories
@SpringBootApplication
public class Application {
	public static void main(String[] args) throws UnsupportedEncodingException {
		String input = "benspassword";
		if (args.length > 0) {
			input = args[0];
		}
		System.out.println("° ° ° ° °   Iniciando Application versao 0.5.1 - input = " + input);
		System.out.println("SHA-1 para " + input + " é " + DigestUtils.sha1Hex(input)
				+ " \nque na representação Base64 fica: " + Base64.encodeBase64String(DigestUtils.sha1(input)));
		LdapSecurityConstants algorithm = LdapSecurityConstants.HASH_METHOD_SHA;
		byte[] credentials = input.getBytes("UTF-8");
		System.out.println("° ° ° ° °   " + PasswordUtil.createStoragePassword(credentials, algorithm));
		SpringApplication.run(Application.class, args);
	}

	public static String getPrincipalStringRepresentation(Object principal) {
		StringBuffer sb = new StringBuffer("[");
		if (principal instanceof LdapUserDetails) {
			sb.append(" LdapUserDetails ");
			sb.append("dn = [");
			sb.append(((LdapUserDetails) principal).getDn());
			sb.append("]");
			// sb.append("authorities = ");
			// ((UserDetails) principal).getAuthorities();
			sb.append("");
		} else if (principal instanceof UserDetails) {
			sb.append(" UserDetails ");
			sb.append("username = ");
			((UserDetails) principal).getUsername();
			// sb.append("");
			// sb.append("authorities = ");
			// ((UserDetails) principal).getAuthorities();
			sb.append("");
		} else {
			if (!"String".equals(principal.getClass().getSimpleName())) {
				sb.append(" " + principal.getClass().getSimpleName() + " ");
			}
			sb.append(principal);
		}
		sb.append("]");
		return sb.toString();
	}
}
