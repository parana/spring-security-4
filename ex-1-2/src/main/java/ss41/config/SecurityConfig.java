package ss41.config;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
// import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
// import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.annotation.web.configurers.FormLoginConfigurer;
// import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
// @EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
// @Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	// @Autowired
	// private UserDetailsService userDetailsService;
	//
	// @Autowired
	// public void configure(AuthenticationManagerBuilder auth) throws Exception
	// {
	// auth.userDetailsService(userDetailsService);
	// }

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.AuthorizedUrl authorizedUrl = null;
		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry urlRegistry = null;
		urlRegistry = http.authorizeRequests();
		//
		authorizedUrl = urlRegistry.antMatchers("/mngt/**");
		urlRegistry = authorizedUrl.access("hasRole('ROLE_ADMIN') or hasRole('ROLE_DEVELOPERS')");
		//
		authorizedUrl = urlRegistry.antMatchers("/admin/**");
		urlRegistry = authorizedUrl.access("hasRole('ROLE_ADMIN') or hasRole('ROLE_DEVELOPERS')");
		//
		authorizedUrl = urlRegistry.antMatchers("/super/**");
		urlRegistry = authorizedUrl.access("hasRole('ROLE_ADMIN') or hasRole('ROLE_SUPER')");
		//
		HttpSecurity httpSecurity = urlRegistry.and();
		//
		FormLoginConfigurer<HttpSecurity> formLoginConfigurer = httpSecurity.formLogin();
		System.out.println("° ° ° ° formLoginConfigurer = " + formLoginConfigurer.toString());
	}

	@Configuration
	protected static class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {

		@Override
		public void init(AuthenticationManagerBuilder auth) throws Exception {
			// String urlString = "ldap://springframework.org:389/dc=springframework,dc=org";
			// auth.ldapAuthentication().userDnPatterns("uid={0},ou=people").groupSearchBase("ou=groups").contextSource()
			// 		.url(urlString);
			auth.ldapAuthentication().userDnPatterns("uid={0},ou=people").groupSearchBase("ou=groups").contextSource()
					.ldif("classpath:test-server.ldif");
		}
	}
}
