#!/bin/bash

set -e

APP_NAME=spring-boot-spring-security-ldap
# Projeto Eclipse : spring-boot-spring-security-ldap
if [ $1 = 'build' ];
then
  mvn clean package install
fi
# 
mvn spring-boot:run -Drun.jvmArguments='-Dserver.port=8088'

